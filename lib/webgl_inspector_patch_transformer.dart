import 'dart:async';

import 'package:barback/barback.dart';

/**
 * Transformer allowing the Dart2js generated code to work with WebGL Inspector.
 * For a description of the issue see https://code.google.com/p/dart/issues/detail?id=3351
 * 
 * It inserts a few lines of script to the project HTML files to patch the WebGLRenderingContext wrapper returned by WebGL Inspector.
 */
class WebGLInspectorPatchTransformer extends Transformer {
  static const PATCH_SCRIPT = '''
<script>
  if(window.gli && window.gli.host) {
    var originalInspectContext = gli.host.inspectContext;
    gli.host.inspectContext = function (canvas, rawgl, options) {
      var ret = originalInspectContext(canvas, rawgl, options);
      ret.constructor = {name: 'WebGLRenderingContext'};
      return ret;
    };
    console.log('WebGL-Inspector patched');
  } else {
    console.log('WebGL-Inspector not detected');
  }
</script>
''';

  /// Required named constructor.
  WebGLInspectorPatchTransformer.asPlugin();

  String get allowedExtensions => '.html';

  Future apply(Transform transform) {
    return transform.primaryInput.readAsString().then((content) {
      final AssetId id = transform.primaryInput.id;
      final String patchedContent = content.replaceFirst('<head>', '<head>\n$PATCH_SCRIPT');
      transform.addOutput(new Asset.fromString(id, patchedContent));
      print('WebGLInspectorPatchTransformer - file patched : $id');
    });
  }
}
