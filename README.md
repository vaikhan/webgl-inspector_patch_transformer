# WebGL Inspector patch transformer #

Transformer allowing the Dart2js generated code to work with WebGL Inspector.

For a description of the issue see https://code.google.com/p/dart/issues/detail?id=3351

It inserts a few lines of script to the project HTML files to patch the WebGLRenderingContext wrapper returned by WebGL Inspector.
``` javascript
if(window.gli && window.gli.host) {
  var originalInspectContext = gli.host.inspectContext;
  gli.host.inspectContext = function (canvas, rawgl, options) {
    var ret = originalInspectContext(canvas, rawgl, options);
    ret.constructor = {name: 'WebGLRenderingContext'};
    return ret;
  };
  console.log('WebGL-Inspector patched');
} else {
  console.log('WebGL-Inspector not detected');
}
```

## Usage

Simply add the following lines to your `pubspec.yaml`:

    dependencies:
      webgl_inspector_patch_transformer: any
    transformers:
      - webgl_inspector_patch_transformer